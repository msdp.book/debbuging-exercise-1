def calculate_total_sales(sales_data):
    total_sales = 0
    for category, sales in sales_data.items():
        total_sales += sales
    return total_sales

sales_data = {"electronics": 10000, "books": 5000, "clothing": "7000", "toys": 3000},
total_sales = calculate_total_sales(sales_data)
print(f"Total sales: ${total_sales}")